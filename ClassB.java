public class ClassB implements CommonInterface
{
    @Override
    public void doCommonAction()
    {
        System.out.println("ClassB doing common");
    }

    public void doB()
    {
        System.out.println("ClassB doing B");
    }
}
