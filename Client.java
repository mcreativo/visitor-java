import java.util.List;

public class Client
{
    public void traverseCollection(List<CommonInterface> collection)
    {
        collection.stream().forEach((s) -> {
            if (s instanceof ClassA)
                ((ClassA) s).doA();
            if (s instanceof ClassB)
                ((ClassB) s).doB();
        });
    }
}
