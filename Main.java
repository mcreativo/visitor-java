import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class Main
{
    public static void main(String[] args)
    {
        List<CommonInterface> collection = new ArrayList<>();
        Random random = new Random();
        random.ints(0, 2).limit(10).mapToObj((i) -> {
            return i == 0 ? new ClassA() : new ClassB();
        }).forEach(collection::add);

        Client client = new Client();
        client.traverseCollection(collection);
    }
}
