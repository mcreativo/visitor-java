public class ClassA implements CommonInterface
{
    @Override
    public void doCommonAction()
    {
        System.out.println("ClassA doing common");
    }

    public void doA()
    {
        System.out.println("ClassA doing A");
    }
}
